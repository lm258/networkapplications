(function(){
    'use strict';

    var app  = angular.module(
        'ChatService'
    );
    var chat = new Chat( location.origin.replace(/^http/, 'ws') );

    app.service(
        'Chat',
        [
            '$q',
            function( $q ){

                //Set out chat handler
                this.chat_ = chat;

                //Function returns a promise for authentication
                this.authenticate = function ( username, password ) {
                    chat.authenticate( username, password );
                    return $q( function( resolve, reject ) {
                        chat.onAuthenticationChange = function( data ) {
                            if( data.status == "success" ) {
                                chat.authenticated = true;
                                resolve();
                            } else {
                                chat.authenticated = false;
                                reject( data.status );
                            }
                        }
                    });
                };

                //Function returns a promise for registration
                this.register = function ( username, password ) {
                    chat.register( username, password );
                    return $q( function( resolve, reject ) {
                        chat.onRegister = function( data ) {
                            if( data.status == "success" ) {
                                resolve();
                            } else {
                                reject( data.status );
                            }
                        }
                    });
                };

                //Check if we are authenticated
                this.isAuthenticated = function() {
                    return chat.authenticated;
                }

                //Send a message
                this.send = function( channel, message ) {
                    chat.sendMessage( channel, message );
                }
            }
        ]
    );

})( angular, Chat )