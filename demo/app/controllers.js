(function(){
    'use strict';

    var app = angular.module(
        'ChatService'
    );

    //Controller for handling the login page
    app.controller(
        'LoginController',
        [
            'Chat',
            '$scope',
            '$rootScope',
            '$state',
            function ( Chat, $scope, $rootScope, $state ) {
                $scope.login = function( username, password ) {

                    //Attempt to login using username and password
                    Chat.authenticate( username, password ).then(
                        function() {
                            //Set the username and go to the chat page if we authenticated
                            $rootScope.username = username;
                            $state.go( 'chat' );
                        },
                        function( status ) {
                            //Otherwise display an error
                            $scope.error = status;
                        }
                    );
                }
            }
        ]
    );

    //Controller for handling the register page
    app.controller(
        'RegisterController',
        [
            '$scope',
            'Chat',
            '$state',
            function ( $scope, Chat, $state ) {
                $scope.register = function( name, username, password, password_confirm ) {
                    if( password != password_confirm ) {
                        $scope.error = "Passwords do not match.";
                        return false;
                    }

                    Chat.register( name, username, password ).then(
                        function() {
                            //Tell the user they managed to register
                            $scope.success = "Success, you can now login.";
                            $scope.error   = null;
                        },
                        function( status ) {
                            //Otherwise display an error
                            $scope.error = status;
                        }
                    );
                }
            }
        ]
    );

    //Controller for handling the chat page
    app.controller(
        'ChatController',
        [
            '$scope',
            'Chat',
            '$location',
            '$anchorScroll',
            function( $scope, Chat, $location, $anchorScroll ) {

                //Select a friend to chat with
                $scope.select = function( friend ) {
                    $scope.selected = friend;
                }

                //Send a message
                $scope.send = function( channel, message ) {
                    Chat.send( channel, message );
                    $scope.message = "";
                }

                //Handle the onMessage event
                Chat.chat_.onMessage = function( data ) {
                    if( $scope.channels[ data.channel ] == undefined ) {
                        $scope.channels[ data.channel ] = [ data ];
                    } else {
                        $scope.channels[ data.channel ].push( data );
                        $scope.$apply();
                    }

                    //Scroll to bottom of the chat history
                    $location.hash('bottom');
                    $anchorScroll();
                }

                //List of demo messages
                $scope.channels = {
                        "sop" : [
                            {
                                "at"      : "2016-02-16T18:01:56.435Z",
                                "message" : "Hello World",
                                "from"    : "sop"
                            },
                            {
                                "at"      : "2016-02-16T18:03:56.435Z",
                                "message" : "Hello World, to you too!",
                                "from"    : "test"
                            }
                        ],
                        "stuart" : [
                            {
                                "at"      : "2016-02-16T18:02:56.435Z",
                                "message" : "Hello World",
                                "from"    : "stuart"
                            },
                            {
                                "at"      : "2016-02-16T18:05:56.435Z",
                                "message" : "Hello World, to you too!",
                                "from"    : "test"
                            }
                        ],
                        "lewis" : [
                            {
                                "at"      : "2016-02-16T18:02:56.435Z",
                                "message" : "Hello World",
                                "from"    : "lewis"
                            },
                            {
                                "at"      : "2016-02-16T18:05:56.435Z",
                                "message" : "Hello World, to you too!",
                                "from"    : "test"
                            }
                        ]
                }

                //List of friends
                $scope.friends = [
                    {
                        "username" : "sop",
                        "name"     : "Sop Femi-Olekanma",
                        "online"   : true
                    },
                    {
                        "username" : "stuart",
                        "name"     : "Stuart Retson",
                        "online"   : false
                    },
                    {
                        "username" : "lewis",
                        "name"     : "Lewis Maitland",
                        "online"   : true
                    }
                ];

                //Select first freind by default
                $scope.select( $scope.friends[0] );
            }
        ]
    );
})( $, angular );