/**
 * User class
*/
function User( _name, _username, _id, _blocked ) {
    this.name = _name;
    this.username = _username;
    this.id = _id;
    this.blocked = _blocked;
}

//Add set for websocket service
User.setWebSocketServer = function( server ) {
    User._wss = server;
}

//Get a currently online user with username or false
User.getOnlineUser = function( username ) {
    var user = false;
    User._wss.clients.forEach( (client) => {
        if( typeof client.user == "object" && client.user.username == username ) {
            user = client.user;
        }
    });
    return user;
}

//Get every user except this one
User.prototype.getOtherUsers = function() {
    var otherUsers = [];
    
    User._wss.clients.forEach( ( client ) => {
        if( client.user != this && typeof client.user == "object" ) {
            otherUsers.push( client.user );
        }
    });
    return otherUsers;
}

//Check if a user is blocking me
User.prototype.blockingUser = function( username ) {
    for( var i = 0; i < this.blocked.length; i++ ) {
        if( this.blocked[i].username == username ) {
            return true;
        }
    }
    return false;
}

//Sends a message to both the recipients and the initiator
User.prototype.sendMessage = function( message, to ) {
    //Find user and send them the message
    User._wss.clients.forEach( ( client ) => {
        if( typeof client.user == "object" && ( client.user.username == to || client.user == this ) ) {

            //If they are not blocking us or we are not blocking them
            if( !client.user.blockingUser( this.username ) && !this.blockingUser( to ) ) {
                client.send(JSON.stringify(
                    {
                        "message" : {
                            "message" : message,
                            "from" :    this.username,
                            "channel" : to,
                            "at" : new Date() 
                        }
                    }
                ));
            }
        }
    });
}

module.exports = User;