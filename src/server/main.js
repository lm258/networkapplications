var WebSocketServer = require('ws').Server;
var connect         = require('connect');
var serveStatic     = require('serve-static');
var ChatHandler     = require('./ChatHandler.js');
var mysql           = require('mysql');

//Database URL
var DATABASE = "mysql://bc4d4223c90a75:495df9f5@us-cdbr-iron-east-03.cleardb.net/heroku_5b2fd3be6dbf2af?reconnect=true";

//Create static file server for HTML,CSS and Js files
var app = connect(); 
app.use( serveStatic('./src') ); 
var server = app.listen( process.env.PORT || 8081 );

//Create the chat handler object
var chatHandler = new ChatHandler({
    server: server
});

/**
 * Create MySQL connection with Heroku Database
 * Handle disconnects be re-connecting.
*/
var connection;
function initializeDatabase() {
    connection = mysql.createConnection( DATABASE );
    connection.connect();
    chatHandler.setConnection( connection );
    connection.on( 'error', function( error ) {
        if( error.code == 'PROTOCOL_CONNECTION_LOST') {
            console.log( "Re-establishing database connection." );
            initializeDatabase();
        } else {
            console.log( error.code );
        }
    });
}
initializeDatabase();