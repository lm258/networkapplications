var WebSocketServer = require('ws').Server;
var User            = require('./User.js');
var UserService     = require('./UserService.js');
var FriendService   = require( './FriendService.js' );

function ChatHandler( opts ) {
    this.wss_ = new WebSocketServer( opts );
    User._wss = this.wss_;
    this.wss_.on( 'connection', (webSocket) => {
        this.onConnection( webSocket );
    });
}

//Used for reconecting to the database
ChatHandler.prototype.setConnection = function( connection ) {
    UserService.setConnection( connection );
    FriendService.setConnection( connection );
}

//Getter for returning web socket server
ChatHandler.prototype.getWebSocketServer = function() {
    return this.wss_;
}

//Helper for sending JSON data to cliejt
ChatHandler.prototype.send = function( webSocket, json ) {
    webSocket.send( JSON.stringify( json ) );
}

//Forward message to recipient
ChatHandler.prototype.onMessage = function( webSocket, data ) {
    webSocket.user.sendMessage( data.message, data.channel );
};

/**
 * Used for handling changing channel state (friends)
*/
ChatHandler.prototype.onChannelsChange = function( webSocket, data ) {
    var that = this;
    if( "add" in data ) {
        FriendService.addFriend( webSocket.user, data[ "add" ] ).then(
            function success() {
                that.send( webSocket, { "channel" : { "status" : "success" } } );
            },
            function failure( error ) {
                that.send( webSocket, { "channel" : { "status" : "Could not add friend." } } );
            }
        );
    } else if( "block" in data ) {
        FriendService.blockUser( webSocket.user, data[ "block" ], 1 ).then(
            function success( result ) {
                webSocket.user.blocked = result;
                that.send( webSocket, { "channel" : { "status" : "success" } } );
            },
            function failure( error ) {
                that.send( webSocket, { "channel" : { "status" : "Could not block user." } } );
            }
        );
    } else if( "unblock" in data ) {
        FriendService.blockUser( webSocket.user, data[ "unblock" ], 0 ).then(
            function success( result ) {
                webSocket.user.blocked = result;
                that.send( webSocket, { "channel" : { "status" : "success" } } );
            },
            function failure( error ) {
                console.log( error );
                that.send( webSocket, { "channel" : { "status" : "Could not unblock user." } } );
            }
        );
    } else if( "remove" in data ) {
        FriendService.deleteFriend( webSocket.user, data[ "remove" ] ).then(
            function success() {
                that.send( webSocket, { "channel" : { "status" : "success" } } );
            },
            function failure( error ) {
                that.send( webSocket, { "channel" : { "status" : "Could not remove friend." } } );
            }
        );
    }
}

/**
 * onRegister.
 * Called when a user attempts to register.
*/
ChatHandler.prototype.onRegister = function( webSocket, data ) {
    var that = this;
    UserService.createUser( data ).then(
        function success() {
            that.send( webSocket, { "register" : { "status" : "success" } } );
        },
        function failure( error ) {
            if( error.code == "ER_DUP_ENTRY" ) {
                that.send( webSocket, { "register" : { "status" : "Username '"+ data.username +"' is taken" } } );
            } else {
                that.send( webSocket, { "register" : { "status" : "Could not register user." } } );
            }
        }
    );
};

/**
 * onAuthenticationChange.
 * Called when a user attempts to authenticate.
*/
ChatHandler.prototype.onAuthenticationChange = function( webSocket, data ) {

    //Make sure user is not already logged in
    if( User.getOnlineUser( data.username ) !== false ) {
        this.send( webSocket, { "authentication" : { "status" : "User is already logged in" } } );
        return;
    }

    //Authenticate the user
    var that = this;
    UserService.authenticateUser( data ).then(
        //If correct user details then create user session and notify other clients
        function success( userData ) {
            var friends = [];
            var blocked = [];
            userData.friends.forEach( (friend) => {
                if( friend.blocked == 1 ) {
                    blocked.push( friend );
                } else {
                    friends.push( friend );
                }
            });
            webSocket.user = new User( userData.name, userData.username, userData.id, blocked.splice(0) );
            that.send( webSocket, { "authentication" : { "status" : "success", "friends" : friends, "blocked" : blocked } } );
            that.wss_.clients.forEach( (client) => {
                if( client != webSocket && typeof client.user == "object" ) {
                    that.send( client, { "users" : client.user.getOtherUsers() } );
                }
            });
        },

        //Otherwise respond with error message
        function failure() {
            that.send( webSocket, { "authentication" : { "status" : "Invalid username or password" } }  );
        }
    );
};

//Handler for client connections
ChatHandler.prototype.onConnection = function( webSocket ) {

    webSocket.on( 'message', ( rawData ) => {
        var data = JSON.parse( rawData );

        if( "channel" in data ) {
            this.onChannelsChange( webSocket, data[ "channel" ] );
        }
        else if( "message" in data ) {
            this.onMessage( webSocket, data[ "message" ] );
        }
        else if( "register" in data ) {
            this.onRegister( webSocket, data[ "register" ] );
        }
        else if( "users" in data ) {
            this.send( webSocket, { "users" : webSocket.user.getOtherUsers() } );
        }
        else if( "keep-alive" in data ) {
            return;
        }
        else if( "authentication" in data ) {
            this.onAuthenticationChange( webSocket, data[ "authentication" ] );
        } else {
            this.send( webSocket, { "error" : "Malformed request object." });
        }
    });

    webSocket.on('close', () => {
        webSocket.user = null;
        this.wss_.clients.forEach( (client) => {
            if( client != webSocket && typeof client.user == "object" ) {
                this.send( client, { "users" : client.user.getOtherUsers() } );
            }
        });
    });
}

module.exports = ChatHandler;