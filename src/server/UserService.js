var FriendService = require( './FriendService.js' );

function UserService() {
}

/**
 * Set the database connection
*/
UserService.setConnection = function ( connection ) {
    UserService.connection_ = connection;
}

/**
 * This function is used for creating a user.
*/
UserService.createUser = function( user ) {
    return new Promise( (resolve, reject) => {
        UserService.connection_.query(
            'INSERT INTO `users` ( name, username, password ) VALUES ( ?, ?, MD5(?) );',
            [ user.name, user.username, user.password ],
            (error, results) => {
                if( error ) {
                    reject( error );
                } else {
                    resolve( true );
                }
            }
        );
    });
}

/**
 * Check if a user already exists in the database
*/
UserService.findUser = function( username ) {
    return new Promise( (resolve, reject) => {
        UserService.connection_.query(
            'SELECT * FROM users WHERE username = ? LIMIT 1',
            [ user.username ],
            (error, results) => {
                if( error || results.length == 0 ) {
                    reject( error );
                } else {
                    resolve( results[0] );
                }
            }
        );
    });
}

/**
 * This function is used for authenticating a user
*/
UserService.authenticateUser = function( user ) {
    return new Promise( (resolve, reject) => {
        UserService.connection_.query(
            'SELECT * FROM users WHERE username = ? AND password = MD5(?)',
            [ user.username, user.password ],
            (error, results) => {
                if( error || results.length == 0 ) {
                    reject( error );
                } else {
                    FriendService.findFriends( results[0] ).then(
                        ( friends ) => {
                            results[0]['friends'] = friends;
                            resolve( results[0] );
                        },
                        ( friend_error ) => {
                            refject( friend_error );
                        }
                    );
                }
            }
        );
    });
}

module.exports = UserService; 
