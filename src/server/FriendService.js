function FriendService() {
}

/**
 * Set the database connection
*/
FriendService.setConnection = function ( connection ) {
    FriendService.connection_ = connection;
}

/**
 * Find the friends of a user
*/
FriendService.findFriends = function( user ) {
    return new Promise( (resolve, reject) => {
        FriendService.connection_.query(
            'SELECT users.id, username, name, blocked FROM users, friends WHERE user_id = ? AND users.id = friend_id',
            [ user.id ],
            (error, results) => {
                if( error ) {
                    reject( error );
                } else {
                    resolve( results );
                }
            }
        );
    });
}

/**
 * Find the blocked friends of a user
*/
FriendService.findBlocked = function( user ) {
    return new Promise( (resolve, reject) => {
        FriendService.connection_.query(
            'SELECT users.id, username, name, blocked FROM users, friends WHERE user_id = ? AND users.id = friend_id AND blocked = 1',
            [ user.id ],
            (error, results) => {
                if( error ) {
                    reject( error );
                } else {
                    resolve( results );
                }
            }
        );
    });
}

/**
 * Add a friend for a user
*/
FriendService.addFriend = function( user, friend ) {
    return new Promise( (resolve, reject) => {
        FriendService.deleteFriend( user, friend ).then(
            () => {
                FriendService.connection_.query(
                    'INSERT INTO friends ( user_id, friend_id ) VALUES ( ?, ? );',
                    [ user.id, friend.id ],
                    (error, results) => {
                        if( error ) {
                            reject( error );
                        } else {
                            resolve( results[0] );
                        }
                    }
                );
            },
            () => {
                reject( error );
            }
        );
    });
}

/**
 * Block a user or friend
*/
FriendService.blockUser = function( user, friend, blocked ) {
    return new Promise( (resolve, reject) => {
        FriendService.deleteFriend( user, friend ).then(
            function success() {
                if( blocked == 1 || blocked == true ) {
                    FriendService.connection_.query(
                        'INSERT INTO friends ( blocked, user_id, friend_id ) VALUES ( ?, ?, ? );',
                        [ blocked, user.id, friend.id ],
                        (error, results) => {
                            if( error ) {
                                reject( error );
                            } else {
                                FriendService.findBlocked( user ).then(
                                    function success( results ) {
                                        resolve( results );
                                    },
                                    function error( error ) {
                                        reject( error );
                                    }
                                );
                            }
                        }
                    );
                } else {
                    FriendService.findBlocked( user ).then(
                        function success( results ) {
                            resolve( results );
                        },
                        function error( error ) {
                            reject( error );
                        }
                    );
                }
            },
            function error( error ) {
                reject( error );
            }
        );
    });
}

/**
 * Delete a friend
*/
FriendService.deleteFriend = function( user, friend ) {
    return new Promise( (resolve, reject) => {
        FriendService.connection_.query(
            'DELETE FROM friends WHERE user_id = ? AND friend_id = ?;',
            [ user.id, friend.id ],
            (error, results) => {
                if( error ) {
                    reject( error );
                } else {
                    resolve( true );
                }
            }
        );
    });
}

module.exports = FriendService; 
