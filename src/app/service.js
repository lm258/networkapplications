(function(){
    'use strict';

    var app  = angular.module(
        'ChatService'
    );
    var chat = new Chat( location.origin.replace(/^http/, 'ws') );

    app.service(
        'Chat',
        [
            '$q',
            function( $q ){

                //Set out chat handler
                this.chat_ = chat;

                //Function returns a promise for authentication
                this.authenticate = function ( username, password ) {
                    chat.authenticate( username, password );
                    return $q( function( resolve, reject ) {
                        chat.onAuthenticationChange = function( data ) {
                            if( data.status == "success" ) {
                                chat.authenticated = true;
                                resolve( data );
                            } else {
                                chat.authenticated = false;
                                reject( data.status );
                            }
                        }
                    });
                };

                //Function returns a promise for registration
                this.register = function ( name, username, password ) {
                    chat.register( name, username, password );
                    return $q( function( resolve, reject ) {
                        chat.onRegister = function( data ) {
                            if( data.status == "success" ) {
                                resolve();
                            } else {
                                reject( data.status );
                            }
                        }
                    });
                };

                //Check if we are authenticated
                this.isAuthenticated = function() {
                    return chat.authenticated;
                }

                //Get the newest users
                this.getUsers = function() {
                    chat.getUsers();
                }

                //Send a message
                this.send = function( channel, message ) {
                    chat.sendMessage( channel, message );
                }

                //Function used to add a friend
                this.addFriend = function ( user ) {
                    chat.addFriend( user );
                    return $q( function( resolve, reject ) {
                        chat.onChannelsChange = function( data ) {
                            if( data.status == "success" ) {
                                resolve( data );
                            } else {
                                reject( data );
                            }
                        }
                    });
                };

                //Function used to remove a friend
                this.removeFriend = function ( user ) {
                    chat.removeFriend( user );
                    return $q( function( resolve, reject ) {
                        chat.onChannelsChange = function( data ) {
                            if( data.status == "success" ) {
                                resolve( data );
                            } else {
                                reject( data );
                            }
                        }
                    });
                };

                //Function used to block a user
                this.blockUser = function ( user ) {
                    chat.blockUser( user );
                    return $q( function( resolve, reject ) {
                        chat.onChannelsChange = function( data ) {
                            if( data.status == "success" ) {
                                resolve( data );
                            } else {
                                reject( data );
                            }
                        }
                    });
                };

                //Function used to unblock a user
                this.unblockUser = function ( user ) {
                    chat.unblockUser( user );
                    return $q( function( resolve, reject ) {
                        chat.onChannelsChange = function( data ) {
                            if( data.status == "success" ) {
                                resolve( data );
                            } else {
                                reject( data );
                            }
                        }
                    });
                };
            }
        ]
    );

})( angular, Chat )