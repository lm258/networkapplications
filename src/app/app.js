(function(){
    'use strict';

    var app = angular.module(
        'ChatService', [ 'ui.router', 'ngCookies' ]
    );

    app.config([
        '$stateProvider',
        '$urlRouterProvider',
        function( $stateProvider, $urlRouterProvider ) {

            $urlRouterProvider.otherwise("/");
            $stateProvider.
            state('login', {
                url: "/",
                templateUrl: "app/views/login.html",
                controller: 'LoginController'
            }).
            state('register', {
                url: "/register",
                templateUrl: "app/views/register.html",
                controller: 'RegisterController'
            }).
            state('chat', {
                url: "/chat",
                templateUrl: "app/views/chat.html",
                controller: 'ChatController'
            });
        }
    ]);

    app.run([
        '$rootScope',
        '$state',
        '$stateParams',
        'Chat',
        function( $rootScope, $state, $stateParams, Chat ) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            $rootScope.$on(
                '$stateChangeStart', 
                function( event, toState, toParams, fromState, fromParams, options ) {
                    if( !Chat.isAuthenticated() && toState.name == "chat" ) {
                        event.preventDefault();
                        $state.go( 'login' );
                    }
                });
        }
    ]);

})($, angular);