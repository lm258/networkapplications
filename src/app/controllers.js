(function(){
    'use strict';

    var app = angular.module(
        'ChatService'
    );

    //Controller for handling the login page
    app.controller(
        'LoginController',
        [
            'Chat',
            '$scope',
            '$rootScope',
            '$state',
            function ( Chat, $scope, $rootScope, $state ) {
                $scope.login = function( username, password ) {

                    //Attempt to login using username and password
                    Chat.authenticate( username, password ).then(
                        function( data ) {
                            //Set the username and go to the chat page if we authenticated
                            $rootScope.username = username;
                            $rootScope.friends  = {};
                            $rootScope.blocked  = {};
                            for( var i = 0; i < data.friends.length; i++ ) {
                                $rootScope.friends[ data.friends[i].username ] = data.friends[i];
                            }
                            for( var i = 0; i < data.blocked.length; i++ ) {
                                $rootScope.blocked[ data.blocked[i].username ] = data.blocked[i];
                            }
                            $state.go( 'chat' );
                        },
                        function( status ) {
                            //Otherwise display an error
                            $scope.error = status;
                        }
                    );
                }
            }
        ]
    );

    //Controller for handling the register page
    app.controller(
        'RegisterController',
        [
            '$scope',
            'Chat',
            '$state',
            function ( $scope, Chat, $state ) {
                $scope.register = function( name, username, password, password_confirm ) {
                    if( password != password_confirm ) {
                        $scope.error = "Passwords do not match.";
                        return false;
                    }

                    Chat.register( name, username, password ).then(
                        function() {
                            //Tell the user they managed to register
                            $scope.success = "Success, you can now login.";
                            $scope.error   = null;
                        },
                        function( status ) {
                            //Otherwise display an error
                            $scope.error = status;
                        }
                    );
                }
            }
        ]
    );

    //Controller for handling the chat page
    app.controller(
        'ChatController',
        [
            '$scope',
            '$rootScope',
            'Chat',
            '$location',
            '$anchorScroll',
            function( $scope, $rootScope, Chat, $location, $anchorScroll ) {

                //Select a friend to chat with
                $scope.select = function( channel ) {
                    if( channel !== undefined && $scope.channels[ channel.username ] == undefined ) {
                        $scope.channels[ channel.username ] = [];
                    }
                    $scope.selected = channel;
                }

                //Logout function
                $scope.logout  = function() {
                    location.reload();
                }

                //Send a message
                $scope.send = function( channel, message ) {
                    Chat.send( channel, message );
                    $scope.message = "";
                }

                $scope.addFriend = function( user ) {
                    Chat.addFriend( user ).then(
                        function success() {
                            $rootScope.friends[ user.username ] = user;
                        },
                        function error() {
                            alert( "User could not be added as friend." );
                        }
                    );
                }

                $scope.removeFriend = function( user ) {
                    Chat.removeFriend( user ).then(
                        function success() {
                            delete $rootScope.friends[ user.username ];
                        },
                        function error() {
                            alert( "User could not be deleted." );
                        }
                    );
                }

                $scope.blockUser = function( user ) {
                    Chat.blockUser( user ).then(
                        function success() {
                            delete $rootScope.friends[ user.username ];
                            $rootScope.blocked[ user.username ] = user;
                        },
                        function error() {
                            alert( "User could not be blocked." );
                        }
                    );
                }

                $scope.unblockUser = function( user ) {
                    Chat.unblockUser( user ).then(
                        function success() {
                            delete $rootScope.blocked[ user.username ];
                        },
                        function error() {
                            alert( "User could not be blocked." );
                        }
                    );
                }

                $scope.empty = function( obj ) {
                    return angular.equals( {}, obj );
                }

                $scope.userOnline = function( user ) {
                    if( typeof $scope.users != "object" ) {
                        return false;
                    }
                    for( var i in $scope.users ) {
                        if( $scope.users[i].username == user.username ) {
                            return true;
                        }
                    }
                    return false;
                }

                //Handle the onMessage event
                Chat.chat_.onMessage = function( data ) {
                    if( data.from != $rootScope.username ) {
                        data.channel = data.from;
                    }
                    if( $scope.channels[ data.channel ] == undefined ) {
                        $scope.channels[ data.channel ] = [ data ];
                    } else {
                        $scope.channels[ data.channel ].push( data );
                        $scope.$apply();
                    }

                    //Scroll to bottom of the chat history
                    $location.hash('bottom');
                    $anchorScroll();
                }

                Chat.getUsers();
                Chat.chat_.onUsersChange = function( data ) {
                    $scope.users = data;
                    if( $scope.selected == undefined ) {
                        $scope.select( $scope.users[0] );
                    }
                    $scope.$apply();
                }

                //List of demo messages
                $scope.channels = {
                }
            }
        ]
    );
})( $, angular );