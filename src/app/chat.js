(function() {
    /**
     * Wrapper class for sending messages over websocket
     * to specific chat channel.
     * Implemented for Network Applications Chat Service.
     * - Lewis Maitland
    */
    window.Chat = function( webSocketURL ) {

        /**
         * Class fields
        */
        var webSocket;
        var authenticated = false;
        var that          = this;

        //Constructor for start chat session with server
        this.constructor = function( webSocketURL ) {

            if( ('WebSocket' in window) === false ) {
                throw "WebSockets are not supported in this browser.";
            };

            //Create new websocket
            that.webSocket = new WebSocket( webSocketURL );

            //Handle message received through websocket
            that.webSocket.onmessage = function( event ) {
                var data = JSON.parse( event.data );

                if( "channel" in data ) {
                    that.onChannelsChange( data[ "channel" ] );
                }
                else if( "message" in data ) {
                    that.onMessage( data[ "message" ] );
                }
                else if( "register" in data ) {
                    that.onRegister( data[ "register" ] );
                }
                else if( "authentication" in data ) {
                    that.onAuthenticationChange( data[ "authentication" ] );
                }
                else if( "users" in data ) {
                    that.onUsersChange( data[ "users" ] );
                }
                else if( "error" in data ) {
                    that.onError( data[ "error" ] );
                } else {
                    that.onError( { "message" : "Malformed request", "data" : data } );
                }
            }

            //Send keepalive every 10 seconds
            setInterval(
                () => {
                    this.webSocket.send( JSON.stringify(
                        { "keep-alive" : {} }
                    ));
                },
                10000
            );

            //Handle error on websocket
            that.webSocket.onerror = function( event ) {
                that.onError( { "message" : "WebSocket error" } );
            }

            //Handle onOpen on websocket
            that.webSocket.onopen = function( event ) {
                that.onOpen();
            }
        };

        //Register as a new user on the system
        this.register = function( name, username, password ) {
            that.webSocket.send( JSON.stringify({
                "register" : {
                    "name"     : name,
                    "username" : username,
                    "password" : password
                }
            }) );
        };

        //Authenticate with the system (login)
        this.authenticate = function( username, password ) {
            that.webSocket.send( JSON.stringify({
                "authentication" : {
                    "username" : username,
                    "password" : password
                }
            }) );
        };

        //Method for sending a message
        this.sendMessage = function( channel, message ) {
            that.isAuthenticatedOrFail();
            that.webSocket.send( JSON.stringify({
                "message" : {
                    "channel" : channel,
                    "message" : message
                }
            }) );
        };

        //Create a channel
        this.createChannel = function( channelName ) {
            that.isAuthenticatedOrFail();
            that.webSocket.send( JSON.stringify({
                "channel" : {
                    "name"   : channelName,
                    "action" : "CREATE"
                }
            }) );
        };

        //Join a channel
        this.addFriend = function( user ) {
            that.isAuthenticatedOrFail();
            that.webSocket.send( JSON.stringify({
                "channel" : {
                    "add" : user
                }
            }) );
        };

        //Leave a channel- if chanel creator and emopty then destroy channel
        this.blockUser = function( user ) {
            that.isAuthenticatedOrFail();
            that.webSocket.send( JSON.stringify({
                "channel" : {
                    "block" : user
                }
            }) );
        };

        //Leave a channel- if chanel creator and emopty then destroy channel
        this.unblockUser = function( user ) {
            that.isAuthenticatedOrFail();
            that.webSocket.send( JSON.stringify({
                "channel" : {
                    "unblock" : user
                }
            }) );
        };

        //Remove a friend
        this.removeFriend = function( user ) {
            that.isAuthenticatedOrFail();
            that.webSocket.send( JSON.stringify({
                "channel" : {
                    "remove" : user
                }
            }) );
        };

        //Authenticate with the system (login)
        this.getUsers = function() {
            that.webSocket.send( JSON.stringify({
                "users" : {
                }
            }) );
        };

        //Simple function to check authentication otherwise throw exception
        this.isAuthenticatedOrFail = function() {
            if( that.authenticated !== true ) {
                throw "User is not authenticated.";
            }
        }

        /**
         * Callback functions.
        */
        //Callback when connection has been established
        this.onOpen = function( data ) {
            console.log( "This function should be overriden." );
        }

        //Callback for receiving message
        this.onMessage = function( data ) {
            console.log( "This function should be overriden." );
        };

        //Callback used for authentication callback
        this.onRegister = function( data ) {
            console.log( "This function should be overriden." );
        }

        //Callback used for notifying of new users online
        this.onUsersChange = function( data ) {
            console.log( "This function should be overriden." );
        }

        //Callback is called when authentication changes
        this.onAuthenticationChange = function( data ) {
            console.log( "This function should be overriden." );
        }

        //Callback is called when available channels change
        this.onChannelsChange = function( data ) {
            console.log( "This function should be overriden." );
        }

        //Callback for error
        this.onError = function( error ) {
            throw error;
        }

        this.constructor( webSocketURL ); // Call the constructor
    };
})( window );